# CHANGELOG

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/) and [Keep a Changelog](http://keepachangelog.com/).


## Unreleased
---

### New

### Changes
* Added .exe file for distribution

### Fixes
* Fixed bug filename selection for output

### Breaks


## 0.0.1 - (2021-09-16)
---

### New
* Initial Release

