HydroGen-GUI
============

HydroGen-GUI is a user interface for the hydrogen package. It is still under heavy development.

Installation
============

If you do not wish to download the source files, you can download

`/dist/HydroGen-GUI.exe`

and extract it to a folder of your choice. Then simply run `HydroGen-GUI.exe`