import setuptools
import sys

def readme():
    with open('README.rst') as f:
        return f.read()

install_requires = []

setuptools.setup(
      name='hydrogen-gui',
      version='0.0.1',
      description='GUI for the hydrogen package: generate stochastic domestic water consumption events',
      long_description=readme(),
      url='https://gitlab.com/brunohad/HydroGen-GUI',
      author='Bruno Hadengue',
      author_email='bruno.hadengue@eawag.ch',
      license='GNU AGPLv3',
      packages=setuptools.find_packages(),
      install_requires=install_requires,
      entry_points ={
            'console_scripts': [
                'hydrogen-gui = HydroGen-GUI.HydroGen-GUI:main']
      },
      test_suite='nose.collector',
      tests_require=['nose'],
      zip_safe=False,
      include_package_data=True,
      options={"bdist_wheel": {"universal": "1"}})
