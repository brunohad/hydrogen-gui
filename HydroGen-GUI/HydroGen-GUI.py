import ast
import os, sys
# Translate asset paths to useable format for PyInstaller

from PyQt5.QtWidgets import (QApplication, QMainWindow, QFileDialog, QWidget, QToolButton, QLineEdit, QLabel, QSplashScreen)
from PyQt5.QtCore import QEvent, QSize, Qt
from PyQt5.QtGui import QIcon, QTextCursor, QIntValidator, QFont, QPixmap, QMouseEvent
from PyQt5.Qt import QUrl, QDesktopServices
import pandas.plotting._matplotlib
import matplotlib
import matplotlib.backends.backend_tkagg
matplotlib.use('tkagg')

# Handle high resolution displays:
if hasattr(Qt, 'AA_EnableHighDpiScaling'):
    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True)
if hasattr(Qt, 'AA_UseHighDpiPixmaps'):
    QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps, True)

try:
    import ctypes
    myappid = 'Eawag.HydroGen.version'
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
except ImportError:
    pass

# from importlib import reload
from MainWindow_ui import Ui_MainWindow
# from TabWidgetTemplate_ui import Ui_TabWidgetTemplate
from TabWidgetTemplate_ui import Ui_TabWidgetTemplate
# reload(MainWindow_ui.Ui_MainWindow)

# from hydrogen.tools.tools import initRead
from json import load, dump
from ast import literal_eval
from datetime import datetime
from time import sleep
from hydrogen.HydroGen import hydrograph


def resource_path(relative_path):
  if hasattr(sys, '_MEIPASS'):
      return os.path.join(sys._MEIPASS, relative_path)
  return os.path.join(os.path.abspath('..'), relative_path)

# class TabWidgetTemplate(QWidget, Ui_TabWidgetTemplate):
#     def __init__(self, parent=None):
#         QWidget.__init__(self, parent)
#         self.setupUi(self)

class TabWidgetTemplate(QWidget, Ui_TabWidgetTemplate):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.setupUi(self)

class Window(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(resource_path('./ui/resources/HydroGenIcon.ico')))
        self.setWindowTitle("Hydrogen")
        with open(resource_path('./ui/resources/variableDict.json'), 'r') as f:
            self.variableDict = load(f)

        self.currentFilename = ""
        self.saveFilename = ""
        self.fileNameDistroFile = ""
        self.hydroGenLogo = resource_path('./ui/resources/HydroGenLogo.jpg')
        document = self.textEdit.document()
        cursor = QTextCursor(document)
        p1 = cursor.position()  # returns int
        cursor.insertImage(self.hydroGenLogo)
        self.updateTextEdit("Welcome to the HydroGen interface.\n\nEnable the help mode with `Ctrl + H` or via the Help menu and click on any variable to get guidance")
        self.init = {"distroFile": {}, "eventList": []}  # dummy init to generate tab
        self.eventListDictTemplate = {"type": "type",
                        "usage": "usage",
                        "nbEvents": {"method": "truncNorm"},
                        "flowDist": {"dist": "truncNorm"},
                        "volumeDist": {"dist": "truncNorm"},
                        "tempDist": {"dist": "truncNorm"},
                        "opEnDist": {"dist": "truncNorm"}}
        self.connectSignalsSlots()
        self.addTab()

        # Tab section
        self.tabWidgetUsages.tabBar().installEventFilter(self)
        self.tabWidgetUsages.add_button = QToolButton(self.tabWidgetUsages, text="+")
        self.tabWidgetUsages.add_button.clicked.connect(self.addTab)
        self.tabWidgetUsages.tabCloseRequested.connect(self.removeTab)


    def enterHelpMode(self):
        # Main parameters
        self.labelTotSimTime.mousePressEvent = lambda event, label="Period duration in seconds (set to 86400 for one day)": self.doSomething(event, label)
        self.labelSimDays.mousePressEvent = lambda event, label="Number of periods (days) you would like to simulate": self.doSomething(event, label)
        self.labelNbInhabitants.mousePressEvent = lambda event, label="Number of inhabitants in the household as an integer value. You can also set a list of floats, in which case the number of inhabitants will be sampled from a truncated normal distribution with parameters [mean, standard deviation, lower bound, upper bound": self.doSomething(event, label)
        self.labelOpEnergy.mousePressEvent = lambda event, label="Set whether the appliance uses operational energy or not (for instance electricity consumption for a dishwasher)": self.doSomething(event, label)
        self.labelDistro.mousePressEvent = lambda event, label="Frequency curves used as probability distributions for the timing of the events. In case you use the Poisson process method for 'Number of Events', make sure that the curves are scaled appropriately": self.doSomething(event, label)
        self.labelDistroFilename.mousePressEvent = lambda event, label="File location for the frequency curves, i.e. the probability curves for each appliance (as an excel file, see the online documentation for more information on the required format)": self.doSomething(event, label)
        self.labelDistroSkip_Rows.mousePressEvent = lambda event, label="Rows to be skipped when reading the file containing frequency curves, for instance [1:10] to skip rows 1 to 10": self.doSomething(event, label)
        self.labelDistroUse_cols.mousePressEvent = lambda event, label="Columns to be used in the file containing frequency curves, for instance 'A:C, E' (with quotation marks) to use columns A to C, and E in the file": self.doSomething(event, label)
        self.labelOutputFile.mousePressEvent = lambda event, label="Location of the output file. Leave blank if you do not wish to generate any file": self.doSomething(event, label)

        # Usage parameters
        for i in range(0, self.tabWidgetUsages.count(), 1):
            tab = self.tabWidgetUsages.widget(i)
            tab.labelUsage.mousePressEvent = lambda event, label="Usage name, for instance `AdultUsage`, `ChildUsage` or even `Shave`": self.doSomething(event, label)
            tab.labelType.mousePressEvent = lambda event, label="Type name, for instance `Shower`, `Tap` or `Dishwasher`. Should correspond to a column in the frequency curves file": self.doSomething(event, label)
            tab.labelnbEvent.mousePressEvent = lambda event, label="Sampling distribution for the number of events in the given simulation period. See online help for more details on available distributions": self.doSomething(event, label)
            tab.labelFlowDist.mousePressEvent = lambda event, label="Sampling distribution for the flow rate of each event. See online help for more details on available distributions": self.doSomething(event, label)
            tab.labelVolumeDist.mousePressEvent = lambda event, label="Sampling distribution for the total volume of each event. The flow rate and the volume are used together to derive the duration of each event. See online help for more details on available distributions": self.doSomething(event, label)
            tab.labeltempDist.mousePressEvent = lambda event, label="Sampling distribution for the required temperature of each event. See online help for more details on available distributions": self.doSomething(event, label)
            tab.labelopEnDist.mousePressEvent = lambda event, label="Sampling distribution for the total operational energy of each event. See online help for more details on available distributions": self.doSomething(event, label)

    def quitHelpMode(self):
        self.labelTotSimTime.mousePressEvent = self.doNothing
        self.labelSimDays.mousePressEvent = self.doNothing
        self.labelNbInhabitants.mousePressEvent = self.doNothing
        self.labelOpEnergy.mousePressEvent = self.doNothing
        self.labelDistro.mousePressEvent = self.doNothing
        self.labelDistroFilename.mousePressEvent = self.doNothing
        self.labelDistroSkip_Rows.mousePressEvent = self.doNothing
        self.labelDistroUse_cols.mousePressEvent = self.doNothing

        for i in range(0, self.tabWidgetUsages.count(), 1):
            tab = self.tabWidgetUsages.widget(i)
            tab.labelUsage.mousePressEvent = self.doNothing
            tab.labelType.mousePressEvent = self.doNothing
            tab.labelnbEvent.mousePressEvent = self.doNothing
            tab.labelFlowDist.mousePressEvent = self.doNothing
            tab.labelVolumeDist.mousePressEvent = self.doNothing
            tab.labeltempDist.mousePressEvent = self.doNothing
            tab.labelopEnDist.mousePressEvent = self.doNothing

    def doSomething(self, QMouseEvent, label):
        self.updateTextEdit(label)

    def doNothing(self, event):
        pass

    def addTab(self):
        self.usageUpdate()
        self.init['eventList'].append(self.eventListDictTemplate)
        self.usageInit()
        self.tabWidgetUsages.setCurrentIndex(self.tabWidgetUsages.count()-1)

    def eventFilter(self, obj, event):
        if obj is self.tabWidgetUsages.tabBar() and event.type() == QEvent.Resize:
            r = self.tabWidgetUsages.tabBar().geometry()
            h = r.height()
            self.tabWidgetUsages.add_button.setFixedSize((h - 1) * QSize(1, 1))
            self.tabWidgetUsages.add_button.move(r.right()-50, 0)
        return super().eventFilter(obj, event)

    def connectSignalsSlots(self):
        # Menu Actions
        self.actionQuit.triggered.connect(self.close)
        self.actionOpen.triggered.connect(self.openFile)
        self.actionSave.triggered.connect(self.saveFile)
        self.actionSave_as.triggered.connect(self.saveFileAs)
        self.actionNew.triggered.connect(self.reset)
        # QWhatsThis.enterWhatsThisMode()
        self.actionHydrogen_Help.triggered.connect(lambda: self.enterHelpMode() if self.actionHydrogen_Help.isChecked() else self.quitHelpMode())
        self.actionOnline_Docs.triggered.connect(lambda: QDesktopServices.openUrl(QUrl("https://hydrogen20.readthedocs.io/en/latest/index.html")))

        # LineEdits update
        self.lineEditTotSimTime.setValidator(QIntValidator())
        self.lineEditTotSimTime.editingFinished.connect(lambda: self.updateValueLineEdit_integer(self.lineEditTotSimTime, 'totSimTime'))
        self.lineEditsimDays.setValidator(QIntValidator())
        self.lineEditsimDays.editingFinished.connect(lambda: self.updateValueLineEdit_integer(self.lineEditsimDays, 'simDays'))
        self.lineEditnbInhabitants.setValidator(QIntValidator())
        self.lineEditnbInhabitants.editingFinished.connect(lambda: self.updateValueLineEdit_integer(self.lineEditnbInhabitants, 'nbInhabitants'))
        self.comboBoxoperationEnergy.currentIndexChanged.connect(lambda: self.updateValueComboBox(self.comboBoxoperationEnergy, 'operationEnergy'))

        # Distro line edits
        self.lineEditDistroFilename.editingFinished.connect(
            lambda: self.updateValueLineEdit("""self.init["distroFile"]["fileName"]='{}'""".format(self.lineEditDistroFilename.text())))
        self.lineEditDistroSkip_Rows.editingFinished.connect(
            lambda: self.updateValueLineEdit("""self.init["distroFile"]["skip_rows"]={}""".format(self.lineEditDistroSkip_Rows.text())) if self.customLiteralEval(self.lineEditDistroSkip_Rows.text()) else None)
        self.lineEditDistroUse_cols.editingFinished.connect(
            lambda: self.updateValueLineEdit("""self.init["distroFile"]["use_cols"]='{}'""".format(self.lineEditDistroUse_cols.text())))
        self.toolButton.clicked.connect(self.getFileNameDistroFile)
        self.outputFileTooButton.clicked.connect(self.getFileNameOutputFile)

        # Tabs
        self.tabWidgetUsages.currentChanged.connect(self.setTabSignals)

        # Simulate Button
        self.pushButtonSimulate.clicked.connect(self.simulate)

    def reset(self):
        self.currentFilename = ""
        self.saveFilename = ""
        self.fileNameDistroFile = ""
        self.updateTextEdit("New initialization file")
        del(self.init)
        self.init = {"distroFile": {}, "eventList": []}  # dummy init to generate tab
        self.init['eventList'].append(self.eventListDictTemplate)
        self.updateAll()

    def reconnect(self, signal, newhandler=None, oldhandler=None):
        try:
            if oldhandler is not None:
                while True:
                    signal.disconnect(oldhandler)
            else:
                signal.disconnect()
        except TypeError:
            pass
        if newhandler is not None:
            signal.connect(newhandler)

    def setTabSignals(self):
        tab = self.tabWidgetUsages.currentWidget()
        if tab is not None:
            try:
                self.reconnect(tab.comboBox_nbEvents.currentIndexChanged, newhandler=lambda: self.variablesUpdate(tab, 'nbEvents', tab.comboBox_nbEvents.currentText()))
                self.reconnect(tab.comboBox_flowDist.currentIndexChanged, newhandler=lambda: self.variablesUpdate(tab, 'flowDist', tab.comboBox_flowDist.currentText()))
                self.reconnect(tab.comboBox_volumeDist.currentIndexChanged, newhandler=lambda: self.variablesUpdate(tab, 'volumeDist', tab.comboBox_volumeDist.currentText()))
                self.reconnect(tab.comboBox_tempDist.currentIndexChanged, newhandler=lambda: self.variablesUpdate(tab, 'tempDist', tab.comboBox_tempDist.currentText()))
                self.reconnect(tab.comboBox_opEnDist.currentIndexChanged, newhandler=lambda: self.variablesUpdate(tab, 'opEnDist', tab.comboBox_opEnDist.currentText()))
                self.reconnect(tab.lineEdit_usage.editingFinished, newhandler=lambda: self.tabWidgetUsages.setTabText(self.tabWidgetUsages.currentIndex(), tab.lineEdit_usage.text()))

            except Exception as e:
                self.updateTextEdit("setTabSignals: {}".format(e))

    def customLiteralEval(self, strList):
        try:
            list = literal_eval(strList)
            return True
        except Exception as e:
            self.updateTextEdit("problem with skip_rows, please check that '{}' is correctly formatted. skip_rows set to 'None'...\n{}".format(strList, e))
            return False

    def updateValueLineEdit_integer(self, lineEdit, attr):
        self.init[attr] = int(lineEdit.text())

    def updateTextEdit(self, str):
        self.textEdit.append("\n{} - {}".format(datetime.now().strftime("%H:%M:%S"), str))

    def updateValueLineEdit(self, stringExec):
        try:
            exec(stringExec)
        except Exception as e:
            self.updateTextEdit(
                "problem with init update...\n{}\n{}".format(stringExec, e))

    def updateValueComboBox(self, comboBox, attr):
        self.init[attr] = str(comboBox.currentText())
        self.updateAll()

    def openFile(self):
        self.currentFilename, _ = QFileDialog.getOpenFileName(self, 'Open file', './', "HydroGen Init Files (*.in);;All Files (*.*)")
        try:
            with open(self.currentFilename, 'r') as f:
                self.init = load(f)
                # for i in range(0, len(self.init['eventList']), 1):
                    # try:
                    # print(self.init['eventList'][i]["opEnDist"])
                    # except KeyError:
                    #     self.init['eventList'][i]["opEnDist"] = {"dist": "truncNorm"}
            self.updateAll()
            self.saveFilename = self.currentFilename
            self.updateTextEdit("Loaded: {}".format(self.currentFilename))
        except Exception as e:
            self.updateTextEdit("There is a problem with the loading of {}\n{}".format(self.currentFilename, e))

    def saveFile(self):
        try:
            self.usageUpdate()
            if self.saveFilename == "":
                self.saveFileAs()
            else:
                with open(self.saveFilename, "w") as f:
                    dump(self.init, f, indent=3)
                self.updateTextEdit("Saved under '{}'".format(self.saveFilename))
        except Exception as e:
            self.updateTextEdit("Could not save file: {}".format(e))

    def saveFileAs(self):
        self.usageUpdate()
        self.saveFilename, _ = QFileDialog.getSaveFileName()
        if self.saveFilename != "":
            self.saveFile()
        else:
            self.saveFilename = self.currentFilename  # do nothing and go back to status quo if no file is selected (the user clicked cancel)

    def getFileNameDistroFile(self):
        try:
            self.fileNameDistroFile, _ = QFileDialog.getOpenFileName(self, 'Choose fileName')
            self.lineEditDistroFilename.setText(self.fileNameDistroFile)
            self.lineEditDistroFilename.setFocus()  # Need this and the next to trigger the finishedEditing and update
            self.toolButton.setFocus()              # the self.init
        except Exception as e:
            self.updateTextEdit("Problem with getting fileName...\n{}".format(e))

    def updateAll(self):
        try:
            # Main Parameters
            self.lineEditTotSimTime.setText(str(self.init['totSimTime']) if 'totSimTime' in self.init else None)
            self.lineEditsimDays.setText(str(self.init['simDays']) if 'simDays' in self.init else None)
            self.lineEditnbInhabitants.setText(str(self.init['nbInhabitants']) if 'nbInhabitants' in self.init else None)
            self.comboBoxoperationEnergy.setCurrentText(str(self.init['operationEnergy']) if 'operationEnergy' in self.init else "True")

            # DistroFile
            self.lineEditDistroFilename.setText(str(self.init['distroFile']['fileName']) if 'fileName' in self.init['distroFile'] else None)
            self.lineEditDistroSkip_Rows.setText(str(self.init['distroFile']['skip_rows']) if 'skip_rows' in self.init['distroFile'] else None)
            self.lineEditDistroUse_cols.setText(str(self.init['distroFile']['use_cols']) if 'use_cols' in self.init['distroFile'] else None)

            # Usages
            self.usageInit()
        except Exception as e:
            self.updateTextEdit("There was a problem with the initialization file reading...\n{}".format(e))

    def removeTab(self, index):
        widget = self.tabWidgetUsages.widget(index)
        if widget is not None and self.tabWidgetUsages.count() > 1:
            widget.deleteLater()
            self.tabWidgetUsages.removeTab(index)
            self.usageUpdate()

    def removeLastTab(self):
        widget = self.tabWidgetUsages.widget(self.tabWidgetUsages.count())
        if widget is not None and self.tabWidgetUsages.count() > 1:
            widget.deleteLater()
            self.tabWidgetUsages.removeTab(-1)
            self.usageUpdate()

    def usageInit(self):
        self.tabWidgetUsages.clear()
        for usage, i in zip(self.init['eventList'], range(0, len(self.init['eventList']))):
            # scrollbar = QScrollArea(widgetResizable=True)
            # scrollbar.setWidget(TabWidgetTemplate())
            self.tabWidgetUsages.addTab(TabWidgetTemplate(), usage['usage'])
            tab = self.tabWidgetUsages.widget(i)
            tab.lineEdit_type.setText(usage['type'])
            tab.lineEdit_usage.setText(usage['usage'])

            self.setUsage(usage, tab)

    def usageUpdate(self):
        try:
            tmpEventList = []
            for i in range(self.tabWidgetUsages.count()):
                tab = self.tabWidgetUsages.widget(i)
                tmpDict = {}
                tmpDict["type"] = tab.lineEdit_type.text()
                tmpDict["usage"] = tab.lineEdit_usage.text()
                modes = ['nbEvents', 'flowDist', 'volumeDist', 'tempDist']
                if eval(self.comboBoxoperationEnergy.currentText()):
                    modes.append('opEnDist')
                for mode in modes:
                    modeDict = {}
                    attr = getattr(tab, 'comboBox_{}'.format(mode))
                    attr_layout = getattr(tab, 'gridLayout{}'.format(mode))
                    varDict = (self.variableDict['nbEvents'] if mode == 'nbEvents' else self.variableDict['distributions'])
                    try:
                        meth = self.breadcrumb(varDict, attr.currentText())[0]
                    except:
                        pass
                    modeDict['method' if mode == 'nbEvents' else 'dist'] = meth

                    for var, y in zip(varDict[meth]['variables'], range(attr_layout.rowCount())):
                        item = attr_layout.itemAtPosition(y, 1)
                        if item is not None and item.widget().text() != "":
                            modeDict[var] = ast.literal_eval(item.widget().text())

                    tmpDict[mode] = modeDict

                tmpEventList.append(tmpDict)
            self.init['eventList'] = tmpEventList
            self.usageInit()
        except TypeError as e:
            self.updateTextEdit("Something is wrong with your input values, please check: {}".format(e))
            raise Exception("Something is wrong with input file, check 'Operational Energy'")
        except Exception as e:
            self.updateTextEdit("Usage update problem: {}".format(e))
            self.init['eventList'].append = {}
            raise Exception("Something unexpected happened")

    def breadcrumb(self, dictJson, value):
        if dictJson == value:
            return [dictJson]
        elif isinstance(dictJson, dict):
            for k, v in dictJson.items():
                p = self.breadcrumb(v, value)
                if p:
                    return [k] + p
        elif isinstance(dictJson, list):
            lst = dictJson
            for i in range(len(lst)):
                p = self.breadcrumb(lst[i], value)
                if p:
                    return [str(i)] + p

    def setUsage(self, distDict, tab):
        modes = ['nbEvents', 'flowDist', 'volumeDist', 'tempDist']
        if eval(self.comboBoxoperationEnergy.currentText()):
            modes.append('opEnDist')
        for mode in modes:
            tmpDict = self.variableDict['nbEvents'] if mode == 'nbEvents' else self.variableDict['distributions']

            attr = getattr(tab, 'comboBox_{}'.format(mode))
            try:
                method = tmpDict[distDict[mode]["method"] if 'nbEvents' in mode else distDict[mode]["dist"]]

                attr.clear()
                for d in tmpDict.keys():
                    attr.addItem(tmpDict[d]["name"])

                attr.setCurrentText(method["name"])
                self.variablesInit(tab, method["variables"], mode, distDict[mode])
            except KeyError:
                pass # just ignore if there is no opEn dist

    def variablesUpdate(self, tab, mode, methodName):
        varDict = (self.variableDict['nbEvents'] if mode == 'nbEvents' else self.variableDict['distributions'])
        meth = self.breadcrumb(varDict, methodName)[0]
        attrLayout = getattr(tab, 'gridLayout{}'.format(mode))
        self.removeWidgets(attrLayout)

        variables = varDict[meth]['variables']
        for var, vari in zip(variables, range(len(variables))):
            attrLayout.addWidget(QLabel(var, objectName="label{}".format(var), minimumWidth=80, alignment=Qt.AlignRight, font=QFont('Arial', 9)), vari, 0)
            attrLayout.addWidget(QLineEdit(objectName="lineEdit{}".format(var), maximumHeight=15, font=QFont('Arial', 9)), vari, 1)

    def removeWidgets(self, layout):
        for i in reversed(range(layout.count())):
            widgetToRemove = layout.itemAt(i).widget()
            layout.removeWidget(widgetToRemove)
            widgetToRemove.setParent(None)

    def variablesInit(self, tab, varDict, mode, distDict):
        attrLayout = getattr(tab, 'gridLayout{}'.format(mode))
        self.removeWidgets(attrLayout)

        # populate layout
        for var, vari in zip(varDict, range(len(varDict))):
            attrLayout.addWidget(QLabel(var, objectName="label{}".format(var), minimumWidth=80, alignment=Qt.AlignRight, font=QFont('Arial', 9)), vari, 0)
            attrLayout.addWidget(QLineEdit(str(distDict[var]) if var in distDict else None, objectName="lineEdit{}".format(var), maximumHeight=15, font=QFont('Arial', 9)), vari, 1)

    def simulate(self):
        self.saveFile()
        try:
            self.updateTextEdit("Simulating...")
            hydrograph(self.saveFilename, self.lineEditOutputFile.text(), plot=self.checkBoxPlot.checkState(), conv_modelica=self.checkBoxConvertModelica.checkState())
            self.updateTextEdit("Done.")
        except Exception as e:
            self.updateTextEdit("A problem occurred during simulation: \n{}".format(e))

    def getFileNameOutputFile(self):
        try:
            self.FileNameOutputFile, _ = QFileDialog.getSaveFileName(self, 'Output file name', './', "Output Files (*.csv, *.txt)")
            # self.FileNameOutputFile, _ = QFileDialog.g
            self.lineEditOutputFile.setText(self.FileNameOutputFile)
            self.lineEditOutputFile.setFocus()  # Need this and the next to trigger the finishedEditing and update
            self.outputFileTooButton.setFocus()  # the self.init
        except Exception as e:
            self.updateTextEdit("Problem with getting fileName...\n{}".format(e))

def main():

    app = QApplication(sys.argv)

    splash = QSplashScreen(QPixmap(resource_path('./ui/resources/HydroGenLogo.jpg')))
    splash.show()
    app.processEvents()
    sleep(1)

    win = Window()
    win.show()
    splash.finish(win)
    sys.exit(app.exec())


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(str(e))
